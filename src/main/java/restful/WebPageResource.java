package restful;

import crawl.WebPageCrawler;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import model.CrawlingRequest;
import model.WebPage;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static util.WebPageUtil.isValidUrl;

/**
 * {@link WebPage} resource request/response handler.
 *
 * @author shawn
 */
@RestController
public class WebPageResource {
    private WebPageCrawler webPageCrawler;

    public WebPageResource(WebPageCrawler webPageCrawler) {
        this.webPageCrawler = webPageCrawler;
    }

    /**
     * RESTful API crawling a web page.
     *
     * @param request crawling input, e.g. target URL
     * @return BAD_REQUEST response if the request contains invalid URL; otherwise OK response when crawling successfully
     */
    @RequestMapping(value = "/webPage",
            method = POST,
            consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    public ResponseEntity crawlWebPageOn(@RequestBody CrawlingRequest request) {
        String crawlingUrl = request.getCrawlingUrl();

        if (!isValidUrl(crawlingUrl)) {
            return status(BAD_REQUEST)
                    .body("The crawling URL[" + crawlingUrl + "] is invalid; please ensure it meets the following:" +
                            System.lineSeparator() +
                            "1. Absolute path: e.g. http://xxxDomain.xxx" + System.lineSeparator() +
                            "2. HTTP or HTTPS protocol only" + System.lineSeparator() +
                            "3. Non-binary file URL: e.g. https://xxxDomain.xxx/xxx.png is not allowed"
                    );
        }

        WebPage crawled = webPageCrawler.doCrawl(crawlingUrl);

        // TODO: in the future, check if any potential circular parent-child relationship before serializing ...

        return ok(crawled);
    }
}
