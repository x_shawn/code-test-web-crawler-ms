package monitor;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;

import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.*;

/**
 * A health indicator that can be used to monitor the app healthy status at runtime, during deployment process, etc.
 *
 * @author shawn
 */
public class AppHealthIndicator extends AbstractHealthIndicator {
    private static final Path APP_ROOT_PATH = Paths.get(".");

    private final Path crawlDataPath;

    public AppHealthIndicator(String crawlDataFolder) {
        this.crawlDataPath = APP_ROOT_PATH.resolve(crawlDataFolder);
    }

    /**
     * Check the app health, based on the file and directory access permission granted by the runtime file system.
     *
     * @param builder to build the app healthy status, e.g. UP or DOWN
     * @throws Exception
     */
    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        // App root path must be readable and executable in the 1st place
        if (!isReadable(APP_ROOT_PATH) || !isExecutable(APP_ROOT_PATH)) {
            builder.down();
            return;
        }

        // create CRAWL_DATA_PATH if not exists
        if (notExists(crawlDataPath)) {
            createDirectories(crawlDataPath);
            builder.up();
            return;
        }

        // double check the CRAWL_DATA_PATH access in case where other apps had modified it
        if (!isReadable(crawlDataPath) || !isExecutable(crawlDataPath)) {
            builder.down();
            return;
        }

        builder.up();
    }
}
