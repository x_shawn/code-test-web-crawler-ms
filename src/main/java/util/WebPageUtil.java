package util;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.web.util.DefaultUriBuilderFactory;
import org.springframework.web.util.UriBuilderFactory;

import java.net.URI;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.springframework.util.StringUtils.trimTrailingCharacter;

/**
 * A common web page utility providing often-used services, e.g. validate a URL, normalize a URI as web page identity.
 *
 * @author shawn
 */
public final class WebPageUtil {
    // to realise the minimum crawling requirement, here supports HTTP and HTTPS only
    private static final String[] PROTOCOLS_SUPPORTED = { "http", "https" };

    /*
     * For the sake of simplicity in PoC (proof of concept) development, here we hard code the pattern of binary file
     * extensions; as a better alternative approach, it should be maintained separately, e.g. extensions are stored in
     * the external *.properties file and the pattern is built via a service method, etc.
    */
    private static final Pattern BINARY_FILE_EXTENSION = Pattern.compile(
            ".*(\\.(css|js|" +
                    "bmp|gif|jpe?g|png|tiff?|" +
                    "mid|mp2|mp3|mp4|wav|avi|mov|mpeg|ram|m4v|rm|smil|wmv|swf|wma|" +
                    "pdf|zip|rar|gz))$");

    private static final UrlValidator URL_VALIDATOR = new UrlValidator(PROTOCOLS_SUPPORTED);

    private static final UriBuilderFactory URI_BUILDER_FACTORY = new DefaultUriBuilderFactory();

    private static final char TRAILING_SLASH = '/';

    private WebPageUtil() {
    }

    /**
     * For the sake of simplicity in PoC (proof of concept) development, here we determine whether a URL is valid or not,
     * based on its string format and pattern; particularly,
     *
     * <ul>
     *     <li>Non-blank</li>
     *     <li>Absolute path - i.e. {http|https}://{domain}.{ext}</li>
     *     <li>Non-binary content URI, e.g. http://xxx.png is invalid</li>
     * </ul>
     *
     * In real-life development, this may not be sufficient and reliable enough, e.g. there are various binary content
     * extensions on the web and they are continuously changing over time; as such, it would be better to rely on
     * 3rd-party service for both URI format and media content verification, e.g. Cloudinary.
     *
     * @param url to be validated
     * @return TRUE if given url is not blank, non-binary, and in an absolute path; otherwise FALSE
     */
    public static boolean isValidUrl(String url) {
        return isNotBlank(url) && URL_VALIDATOR.isValid(url) && !BINARY_FILE_EXTENSION.matcher(url).matches();
    }

    /**
     * To have a reliable identity for a specific web page, we normalize its URI string via
     *
     * <ul>
     *     <li>trimming any trailing '/' at the end of URI path</li>
     *     <li>encoding the query</li>
     *     <li>dropping any '#' fragment in the end</li>
     * </ul>
     *
     * @param uri to normalize
     * @return the normalized {@link URI}
     */
    public static URI normalizedUriFrom(String uri) {
        URI source = URI_BUILDER_FACTORY.uriString(uri).build();

        return URI_BUILDER_FACTORY.builder()
                .scheme(source.getScheme())
                .host(source.getHost())
                .port(source.getPort())
                .path(trimTrailingCharacter(source.getPath(), TRAILING_SLASH))
                .query(source.getQuery())
                .build();
    }
}
