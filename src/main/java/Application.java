import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
@ComponentScan({  "config", "restful" })
@EnableSwagger2
public class Application {

    public static void main(String[] args) {
        run(Application.class, args);
    }
}
