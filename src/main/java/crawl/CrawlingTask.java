package crawl;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.crawler.WebCrawler;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import persistence.InMemoryPersistence;
import model.WebPage;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;
import static util.WebPageUtil.isValidUrl;

/**
 * Unit of work/task checking if current web page should be crawled; if so, building a {@link WebPage} PoJo and saving it
 * into persistence for later retrieval.
 *
 * @author shawn
 */
public class CrawlingTask extends WebCrawler {
    private InMemoryPersistence persistence;

    public CrawlingTask(InMemoryPersistence persistence) {
        this.persistence = persistence;
    }

    /**
     * Determine if we process the current web page. For the sake of simplicity in PoC development, we do the
     * crawling when given we page URL is valid.
     *
     * NOTE: the crawling decision here is necessary, as it is needed for continuous child page crawling.
     *
     * @param referringPage the fetched we page for crawling
     * @param url the web URL of fetched web page
     * @return TRUE if URL is valid; otherwise FALSE
     */
    @Override
    public boolean shouldVisit(Page referringPage, WebURL url) {
        requireNonNull(url);
        return isValidUrl(url.getURL());
    }

    @Override
    public void visit(Page page) {
        crawlAndSaveWebPage(page);
    }

    /**
     * Crawl the fetched web page; particularly, convert the fetched source {@link Page} into a {@link WebPage} PoJo.
     * Then save it into persistence.
     *
     * @param fetched the source {@link Page} containing raw web content, e.g. title and outgoing URLs.
     * @return the converted and saved {@link WebPage} PoJo.
     */
    public WebPage crawlAndSaveWebPage(Page fetched) {
        // build a new WebPage PoJo, based on the fetched source page
        WebPage crawled = new WebPage(fetched.getWebURL().getURL());
        HtmlParseData htmlDoc = (HtmlParseData) fetched.getParseData();
        crawled.setTitle(htmlDoc.getTitle());

        // set parent-child relationship
        String parentUrl = fetched.getWebURL().getParentUrl();
        if (isValidUrl(parentUrl)) {
            WebPage parentPage = persistence.findByUrl(parentUrl);
            if (nonNull(parentPage)) parentPage.getNodes().add(crawled);
        }

        return persistence.save(crawled);
    }
}
