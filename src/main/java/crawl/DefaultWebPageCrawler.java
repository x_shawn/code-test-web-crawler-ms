package crawl;

import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;
import persistence.InMemoryPersistence;
import model.WebPage;

import static java.lang.Runtime.getRuntime;
import static java.util.Objects.nonNull;

/**
 * Default implementor of {@link WebPageCrawler}, who tries to fetch the previously crawled web page from persistence,
 * prior to start the crawling process.
 *
 * @author shawn
 */
public class DefaultWebPageCrawler implements WebPageCrawler {
    private static final int DEFAULT_CRAWLING_WORKERS = getRuntime().availableProcessors() + 1;

    private CrawlController crawlController;
    private WebCrawlerFactory<CrawlingTask> crawlerFactory;

    private InMemoryPersistence persistence;

    public DefaultWebPageCrawler(CrawlController crawlController, WebCrawlerFactory<CrawlingTask> crawlerFactory,
                                 InMemoryPersistence persistence) {
        this.crawlController = crawlController;
        this.crawlerFactory = crawlerFactory;
        this.persistence = persistence;
    }

    /**
     * Crawl a web page based on the given URL.
     *
     * @param url for crawling
     * @return the crawled and saved web page
     */
    @Override
    public WebPage doCrawl(String url) {
        // firstly, see if the given URL has been crawled before
        WebPage found = persistence.findByUrl(url);
        if (nonNull(found)) return found;

        // then start crawling task (blocking)
        crawlController.addSeed(url);
        crawlController.start(crawlerFactory, DEFAULT_CRAWLING_WORKERS);

        // finally fetch the crawled and saved web page from persistence
        return persistence.findByUrl(url);
    }
}
