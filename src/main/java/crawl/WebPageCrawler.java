package crawl;

import model.WebPage;

/**
 * A web page crawling controller that determines the crawling strategy, e.g. find the earlier saved web page before
 * fetching and crawling.
 *
 * @author shawn
 */
public interface WebPageCrawler {

    /**
     * Crawling a web page with given URL
     *
     * @param url for crawling
     * @return the crawled web page
     */
    WebPage doCrawl(String url);
}
