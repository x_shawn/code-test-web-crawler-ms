package model;

/**
 * PoJo containing the crawling input, e.g. URL.
 *
 * @author shawn
 */
public class CrawlingRequest {
    private String crawlingUrl;

    public void setCrawlingUrl(String crawlingUrl) {
        this.crawlingUrl = crawlingUrl;
    }

    public String getCrawlingUrl() {
        return crawlingUrl;
    }
}
