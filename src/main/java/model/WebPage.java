package model;

import java.util.ArrayList;
import java.util.List;

/**
 * PoJo containing the crawled web page information, e.g. title and URL.
 *
 * @author shawn
 */
public class WebPage {
    private String url;
    private String title;
    private List<WebPage> nodes;

    public WebPage() {
        nodes = new ArrayList<>();
    }

    public WebPage(String url) {
        this.url = url;
        nodes = new ArrayList<>();
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<WebPage> getNodes() {
        return nodes;
    }

    public void setNodes(List<WebPage> nodes) {
        this.nodes = nodes;
    }
}
