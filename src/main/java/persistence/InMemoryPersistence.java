package persistence;

import model.WebPage;

import java.net.URI;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;
import static util.WebPageUtil.isValidUrl;
import static util.WebPageUtil.normalizedUriFrom;

/**
 * A PoC in-memory persistence utility to save and fetch those crawled web pages.
 *
 * @author shawn
 */
public class InMemoryPersistence {

    /*
    * For the sake of simplicity in PoC (Proof of Concept) development, here we use ConcurrentMap for <K, V> pair storage
    * saving, caching, and finding the crawled web pages. In real-life development, we may choose a more mature
    * in-memory storage and caching solution, e.g. EhCache.
    *
    * To save/fetch a crawled web page into/from our <K, V> pair storage, we need a proper K as an identity, which is
    * similar to the ID column/field of DB. As such, we can use the following:
    *
    * 1. UUID or Auto-incremental Number: not suitable for the client to retrieve earlier crawled web page, because our
    * internal storage identity wouldn't and shouldn't be exposed to the external client; as such, the commonly-seen
    * URL/URI should be used as identity instead.
    *
    * 2. URL String: error-prone and not reliable, e.g. 'https://www.google.com/' is not equal to 'https://www.google.com',
    * even though they represent the same URL; and parsing, comparing string is always a pain in a butt ...
    *
    * 3. java.net.URL: conceptually, it makes sense to be used as a web page identity; yet technically, the identity
    * check is not ideal, e.g. 'URL(https://www.google.com.au/search?q=web+crawling)' object is not equal to
    * 'URL(https://www.google.com.au/search?q=web%20crawling)' object whose query fragment is encoded.
    *
    * 4. java.net.URI: it is better than the above, regarding the identity check (and its 'I' for identity makes more
    * sense than the above);
    */
    private static final ConcurrentMap<URI, WebPage> REPOSITORY = new ConcurrentHashMap<>();

    /**
     * Find the previously saved web page from repository.
     *
     * @param url for the existing web page lookup
     * @return the previously saved web page; NULL if no such web page exists
     */
    public WebPage findByUrl(String url) {
        checkArgument(isValidUrl(url));

        URI identity = normalizedUriFrom(url);

        return REPOSITORY.get(identity);
    }

    /**
     * Create or save the crawled web page into repository. If a previously saved web page with the same URL exists in
     * repository, it will be replaced by the current one.
     *
     * @param toSave the crawled web page
     * @return the latest saved web page
     */
    public WebPage save(WebPage toSave) {
        requireNonNull(toSave);

        URI identity = normalizedUriFrom(toSave.getUrl());

        // atomic put operation guaranteed by ConcurrentMap
        REPOSITORY.put(identity, toSave);

        return toSave;
    }

    /**
     * Remove a previously saved web page from repository.
     *
     * @param url for the existing web page lookup
     * @return the previously saved web page being removed; NULL if no such web page exists
     */
    public WebPage remove(String url) {
        WebPage toRemove = findByUrl(url);
        if (isNull(toRemove)) return null;

        URI identity = normalizedUriFrom(url);

        // atomic remove operation guaranteed by ConcurrentMap
        REPOSITORY.remove(identity, toRemove);

        return toRemove;
    }
}
