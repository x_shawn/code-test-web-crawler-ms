package config;

import crawl.CrawlingTask;
import crawl.DefaultWebPageCrawler;
import crawl.WebPageCrawler;
import edu.uci.ics.crawler4j.crawler.CrawlConfig;
import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;
import edu.uci.ics.crawler4j.fetcher.PageFetcher;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtConfig;
import edu.uci.ics.crawler4j.robotstxt.RobotstxtServer;
import monitor.AppHealthIndicator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import persistence.InMemoryPersistence;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;
import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;
import static org.springframework.web.context.WebApplicationContext.SCOPE_REQUEST;

/**
 * Application-wide context and config. e.g. Spring beans and properties.
 *
 * @author shawn
 */
@Configuration
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Value("${crawl.data.folder}")
    private String crawlDataFolder;

    @Value("${crawl.depth}")
    private int crawlDepth;

    @Value("${crawl.outgoing.links}")
    private int crawlOutgoingLinks;

    /**
     * {@link WebPageCrawler} instance needs to be a new one per HTTP request, in order to sustain Crawl4J framework.
     *
     * NOTE: a HTTP request scoped bean can only be supported in Spring web application context; and must be referenced
     * by MVC bean, e.g. a RestController, directly.
     *
     * @return a new {@link WebPageCrawler} bean per HTTP request
     * @throws Exception
     */
    @Bean
    @Scope(value = SCOPE_REQUEST, proxyMode = TARGET_CLASS)
    public WebPageCrawler webPageCrawler() throws Exception {
        return new DefaultWebPageCrawler(
                configCrawlController(),
                configCrawlerFactory(),
                inMemoryPersistence()
        );
    }

    /**
     * {@link CrawlController} must be prototype scoped due to HTTP request scoped {@link WebPageCrawler} bean.
     *
     * @return a new {@link CrawlController} bean per bean reference
     * @throws Exception
     */
    @Bean
    @Scope(value = SCOPE_PROTOTYPE)
    public CrawlController configCrawlController() throws Exception {
        CrawlConfig config = new CrawlConfig();

        // crawling temporary file storage folder
        config.setCrawlStorageFolder(crawlDataFolder);

        // fine-tune the following flags with caution, as they affect performance directly
        config.setMaxDepthOfCrawling(crawlDepth);
        config.setMaxOutgoingLinksToFollow(crawlOutgoingLinks);
        config.setThreadShutdownDelaySeconds(0);
        config.setCleanupDelaySeconds(0);

        PageFetcher pageFetcher = new PageFetcher(config);
        return new CrawlController(config, pageFetcher, new RobotstxtServer(new RobotstxtConfig(), pageFetcher));
    }

    @Bean
    public WebCrawlerFactory<CrawlingTask> configCrawlerFactory() {
        return () -> new CrawlingTask(inMemoryPersistence());
    }

    @Bean
    public InMemoryPersistence inMemoryPersistence() {
        return new InMemoryPersistence();
    }

    @Bean
    public HealthIndicator healthIndicator() {
        return new AppHealthIndicator(crawlDataFolder);
    }

    /**
     * Swagger UI config.
     *
     * @return a {@link Docket} instance
     */
    @Bean
    public Docket configApiDoc() {
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("Web Crawling API")
                .description("A RESTful API used to crawl web URL, page title, and outgoing links.")
                .version("1.0")
                .license("Qantas " + new SimpleDateFormat("yyyy").format(Calendar.getInstance().getTime()))
                .build();

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}
