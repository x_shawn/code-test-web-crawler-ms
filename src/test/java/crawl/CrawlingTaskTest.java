package crawl;

import edu.uci.ics.crawler4j.crawler.Page;
import edu.uci.ics.crawler4j.parser.HtmlParseData;
import edu.uci.ics.crawler4j.url.WebURL;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import persistence.InMemoryPersistence;
import model.WebPage;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CrawlingTaskTest {
    private static final WebURL INVALID_WEB_URL = mockWebUrl("whatever invalid URL");
    private static final WebURL VALID_WEB_URL = mockWebUrl("https://www.google.com.au/search?q=web+crawling");

    private static final String CRAWLED_PAGE_TITLE = "Crawled Page Title";

    @Mock
    private InMemoryPersistence persistence;

    @InjectMocks
    private CrawlingTask crawling;

    @Test(expected = NullPointerException.class)
    public void shouldNotAcceptNullWebUrl() {
        crawling.shouldVisit(null, null);
    }

    @Test
    public void shouldNotCrawlAnyInvalidWebUrl() {
        assertFalse(crawling.shouldVisit(new Page(INVALID_WEB_URL), INVALID_WEB_URL));
    }

    @Test
    public void shouldCrawlAnyValidWebUrl() {
        assertTrue(crawling.shouldVisit(new Page(VALID_WEB_URL), VALID_WEB_URL));
    }

    @Test
    public void shouldCrawlAndSaveWebPage() {
        // GIVEN: a source web page being fetched / downloaded
        Page fetched = mock(Page.class);
        when(fetched.getWebURL()).thenReturn(VALID_WEB_URL);
        HtmlParseData htmlDoc = mock(HtmlParseData.class);
        when(htmlDoc.getTitle()).thenReturn(CRAWLED_PAGE_TITLE);
        when(fetched.getParseData()).thenReturn(htmlDoc);

        // GIVEN: a stubbed persistence returning the crawled and saved web page
        doAnswer(invocation -> invocation.getArguments()[0]).when(persistence).save(any(WebPage.class));

        // WHEN: crawling and saving a web page being mocked as above
        WebPage crawledAndSavedWebPage = crawling.crawlAndSaveWebPage(fetched);

        // THEN: verify the state change - i.e. if the fetched and saved web page PoJo contains the info as mocked
        assertEquals(VALID_WEB_URL.getURL(), crawledAndSavedWebPage.getUrl());
        assertEquals(CRAWLED_PAGE_TITLE, crawledAndSavedWebPage.getTitle());

        // THEN: verify if the above state info comes from the following mock interactions
        verify(fetched, atLeastOnce()).getWebURL();
        verify(fetched).getParseData();
        verify(htmlDoc).getTitle();
        verify(persistence).save(crawledAndSavedWebPage);
    }

    private static WebURL mockWebUrl(String url) {
        WebURL webUrl = mock(WebURL.class);
        when(webUrl.getURL()).thenReturn(url);
        return webUrl;
    }
}
