package crawl;


import edu.uci.ics.crawler4j.crawler.CrawlController;
import edu.uci.ics.crawler4j.crawler.CrawlController.WebCrawlerFactory;
import model.WebPage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import persistence.InMemoryPersistence;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DefaultWebPageCrawlerTest {
    private static final String URL = "whatever valid URL";

    @Mock
    private CrawlController controller;

    @Mock
    private InMemoryPersistence persistence;

    @InjectMocks
    private DefaultWebPageCrawler crawler;

    @Test
    public void shouldReturnWebPageFromPersistenceWhenCrawledAndSavedBefore() {
        // GIVEN: a web page that has been crawled and saved into persistence before
        WebPage crawledBefore = mock(WebPage.class);
        when(persistence.findByUrl(URL)).thenReturn(crawledBefore);

        // WHEN: try to doCrawl the above URL
        WebPage webPage = crawler.doCrawl(URL);

        // THEN: verify the state change - i.e. the earlier crawled web page returned
        assertEquals(crawledBefore, webPage);

        // THEN: verify the following mock interactions causing the above state change
        verify(persistence).findByUrl(URL);
        verify(controller, never()).addSeed(URL);
        verify(controller, never()).start(any(WebCrawlerFactory.class), anyInt());
    }

    @Test
    public void shouldStartCrawlingSeededUrlWhenNotFoundInPersistence() {
        // GIVEN: no web page found from persistence at the beginning; then found after crawling
        WebPage webPage = new WebPage(URL);
        when(persistence.findByUrl(URL)).thenReturn(null).thenReturn(webPage);

        // WHEN: crawling the above URL
        WebPage afterCrawling = crawler.doCrawl(URL);

        // THEN: verify the state change - i.e. the web page being saved after crawling
        assertEquals(webPage, afterCrawling);

        // THEN: verify the following mock interactions causing the above state change
        verify(controller).addSeed(URL);
        verify(controller).start(any(WebCrawlerFactory.class), anyInt());
        verify(persistence, times(2)).findByUrl(URL);
    }
}
