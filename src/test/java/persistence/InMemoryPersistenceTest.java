package persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import model.WebPage;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class InMemoryPersistenceTest {
    private static final String MALFORMED_URL = "whatever URL string that cannot be converted to a valid URI for query";
    private static final String CRAWLED_URL = "https://www.google.com.au/search?q=web%20crawling";
    private static final String NOT_CRAWLED_URL = "https://www.google.com.hk";

    private InMemoryPersistence persistence;

    @Before
    public void initData() {
        persistence = new InMemoryPersistence();
        persistence.save(new WebPage(CRAWLED_URL));
    }

    @After
    public void cleanUpData() {
        persistence.remove(CRAWLED_URL);
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotAcceptIllegalUrlForWebPageQuery() {
        persistence.findByUrl(MALFORMED_URL);
    }

    @Test
    public void shouldReturnNullWhenNoWebPageFound() {
        assertNull(persistence.findByUrl(NOT_CRAWLED_URL));
    }

    @Test
    public void shouldReturnFoundWebPage() {
        WebPage found = persistence.findByUrl(CRAWLED_URL);
        assertEquals(CRAWLED_URL, found.getUrl());
    }

    @Test(expected = NullPointerException.class)
    public void shouldNotSaveNull() {
        persistence.save(null);
    }

    @Test
    public void shouldSaveWebPage() {
        WebPage crawled = new WebPage(CRAWLED_URL);
        WebPage saved = persistence.save(crawled);
        assertEquals(crawled, saved);
    }

    @Test
    public void shouldNotRemoveNonExistingWebPage() {
        assertNull(persistence.remove(NOT_CRAWLED_URL));
    }

    @Test
    public void shouldRemoveExistingWebPage() {
        WebPage removed = persistence.remove(CRAWLED_URL);
        assertEquals(CRAWLED_URL, removed.getUrl());
    }
}
