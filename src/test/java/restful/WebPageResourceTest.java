package restful;

import model.CrawlingRequest;
import model.WebPage;
import crawl.WebPageCrawler;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.http.HttpStatus.*;

@RunWith(MockitoJUnitRunner.class)
public class WebPageResourceTest {
    private static final String INVALID_URL = "whatever invalid URL";
    private static final String VALID_URL = "https://www.google.com.au/search?q=web+crawling";

    @Mock
    private WebPageCrawler webPageCrawler;

    @InjectMocks
    private WebPageResource webPageResource;

    @Test
    public void shouldRespond400BadRequestGivenInvalidUrl() {
        // GIVEN: a bad crawling request containing an invalid URL
        CrawlingRequest badRequest = mock(CrawlingRequest.class);
        when(badRequest.getCrawlingUrl()).thenReturn(INVALID_URL);

        // WHEN: try to get web pages resource as per the above mocks
        ResponseEntity response = webPageResource.crawlWebPageOn(badRequest);

        // THEN: verify the state change: 400 - Bad Request response
        assertEquals(BAD_REQUEST, response.getStatusCode());

        // THEN: verify if the following mock interactions happened to bring about the above state change
        verify(badRequest).getCrawlingUrl();
        verify(webPageCrawler, never()).doCrawl(INVALID_URL);
    }

    @Test
    public void shouldRespond200OKWhenWebPageCrawledSuccessfully() {
        // GIVEN: a crawling request containing a valid URL
        CrawlingRequest request = mock(CrawlingRequest.class);
        when(request.getCrawlingUrl()).thenReturn(VALID_URL);

        // GIVEN: a WebPage being crawled
        WebPage crawledPage = mock(WebPage.class);
        when(webPageCrawler.doCrawl(VALID_URL)).thenReturn(crawledPage);

        // WHEN: try to get web pages resource as per the above mocks
        ResponseEntity response = webPageResource.crawlWebPageOn(request);

        // THEN: verify the state change: 200 - OK response with crawled page
        assertEquals(OK, response.getStatusCode());
        assertEquals(crawledPage, response.getBody());

        // THEN: verify if the following mock interactions happened to bring about the above state change
        verify(request).getCrawlingUrl();
        verify(webPageCrawler).doCrawl(VALID_URL);
    }
}
