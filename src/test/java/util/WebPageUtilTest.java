package util;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.BeforeClass;
import org.junit.Test;

import java.net.URI;
import java.util.List;
import java.util.Map;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static util.WebPageUtil.isValidUrl;
import static util.WebPageUtil.normalizedUriFrom;

public class WebPageUtilTest {
    private static List<String> URLS_WITHOUT_PROTOCOL;
    private static List<String> URLS_WITH_UNSUPPORTED_PROTOCOL;

    private static List<String> MALFORMED_URLS;

    private static List<String> JS_FILE_URLS;
    private static List<String> CSS_FILE_URLS;

    private static List<String> IMAGE_URLS;
    private static List<String> AUDIO_VIDEO_URLS;
    private static List<String> OTHER_BINARY_FILE_URLS;

    private static List<String> WELL_FORMED_URLS;
    
    private static Map<String, URI> URIS_WITH_TRAILING_SLASH;
    private static Map<String, URI> URIS_WITH_QUERIES;
    private static Map<String, URI> URIS_WITH_FRAGMENTS;

    /**
     * NOTE: for the sake of simplicity in PoC (proof of concept) development, here we hard code test data; yet in
     * real-life development, those test data below should come from external source, e.g. file or DB
     */
    @BeforeClass
    public static void initTestData() {
        URLS_WITHOUT_PROTOCOL = ImmutableList.of("www.google.com", "apache.org");
        URLS_WITH_UNSUPPORTED_PROTOCOL = ImmutableList.of("ftp://aaa.xxx", "sftp://bbb.yyy", "file://ccc/ddd");

        MALFORMED_URLS = ImmutableList.of("//www.google.com", "http:apache.org", "http:/australian.gov", "http://abc");

        JS_FILE_URLS = ImmutableList.of("http://xxx.yy/z.js", "https://xxx.yy/z.js");
        CSS_FILE_URLS = ImmutableList.of("http://xxx.yy/z.css", "https://xxx.yy/z.css");

        IMAGE_URLS = ImmutableList.of(
                "http://xxx.yy/z.bmp", "https://xxx.yy/z.bmp",
                "http://xxx.yy/z.gif", "https://xxx.yy/z.gif",
                "http://xxx.yy/z.jpg", "https://xxx.yy/z.jpg",
                "http://xxx.yy/z.jpeg", "https://xxx.yy/z.jpeg",
                "http://xxx.yy/z.png", "https://xxx.yy/z.png",
                "http://xxx.yy/z.tiff", "https://xxx.yy/z.tiff"
        );
        AUDIO_VIDEO_URLS = ImmutableList.of(
                "http://xxx.yy/z.mid", "https://xxx.yy/z.mid",
                "http://xxx.yy/z.mp2", "https://xxx.yy/z.mp2",
                "http://xxx.yy/z.mp3", "https://xxx.yy/z.mp3",
                "http://xxx.yy/z.mp4", "https://xxx.yy/z.mp4",
                "http://xxx.yy/z.wav", "https://xxx.yy/z.wav",
                "http://xxx.yy/z.avi", "https://xxx.yy/z.avi",
                "http://xxx.yy/z.mov", "https://xxx.yy/z.mov",
                "http://xxx.yy/z.mpeg", "https://xxx.yy/z.mpeg",
                "http://xxx.yy/z.ram", "https://xxx.yy/z.ram",
                "http://xxx.yy/z.m4v", "https://xxx.yy/z.m4v",
                "http://xxx.yy/z.rm", "https://xxx.yy/z.rm",
                "http://xxx.yy/z.smil", "https://xxx.yy/z.smil",
                "http://xxx.yy/z.wmv", "https://xxx.yy/z.wmv",
                "http://xxx.yy/z.swf", "https://xxx.yy/z.swf",
                "http://xxx.yy/z.wma", "https://xxx.yy/z.wma"
        );
        OTHER_BINARY_FILE_URLS = ImmutableList.of(
                "http://xxx.yy/z.pdf", "https://xxx.yy/z.pdf",
                "http://xxx.yy/z.zip", "https://xxx.yy/z.zip",
                "http://xxx.yy/z.rar", "https://xxx.yy/z.rar",
                "http://xxx.yy/z.gz", "https://xxx.yy/z.gz"
        );

        WELL_FORMED_URLS = ImmutableList.of("http://apache.org", "https://www.google.com", "http://abc.xyz",
                "https://www.google.com.au/search?q=web+crawling");
        
        URIS_WITH_TRAILING_SLASH = ImmutableMap.of(
            "https://www.google.com/", URI.create("https://www.google.com"),
            "https://www.google.com//", URI.create("https://www.google.com")
        );
        URIS_WITH_QUERIES = ImmutableMap.of(
            "https://www.google.com.au/search?q=web crawling", URI.create("https://www.google.com.au/search?q=web%20crawling")
        );
        URIS_WITH_FRAGMENTS = ImmutableMap.of(
            "https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/html/#packaging-executable-jars",
            URI.create("https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/html")
        );
    }
    
    @Test
    public void shouldFailGivenEmptyUrl() {
        assertFalse(isValidUrl(EMPTY));
    }

    @Test
    public void shouldFailGivenNoProtocolInUrl() {
        URLS_WITHOUT_PROTOCOL.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenUnsupportedProtocolInUrl() {
        URLS_WITH_UNSUPPORTED_PROTOCOL.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenMalformedUrl() {
        MALFORMED_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenAnyJsFileUrl() {
        JS_FILE_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenAnyCssFileUrl() {
        CSS_FILE_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenAnyImageFileUrl() {
        IMAGE_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenAnyAudioVideoFileUrl() {
        AUDIO_VIDEO_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldFailGivenAnyOtherBinaryFile() {
        OTHER_BINARY_FILE_URLS.forEach(url -> assertFalse(isValidUrl(url)));
    }

    @Test
    public void shouldSucceedGivenWellFormedUrl() {
        WELL_FORMED_URLS.forEach(url -> assertTrue(isValidUrl(url)));
    }
    @Test
    public void shouldRemoveTailingSlashAtPathWhenNormalizingUri() {
        URIS_WITH_TRAILING_SLASH.forEach((before, after) -> assertEquals(after, normalizedUriFrom(before)));
    }

    @Test
    public void shouldEncodeQueryParametersWhenNormalizingUri() {
        URIS_WITH_QUERIES.forEach((before, after) -> assertEquals(after, normalizedUriFrom(before)));
    }

    @Test
    public void shouldDropFragmentsWhenNormalizingUri() {
        URIS_WITH_FRAGMENTS.forEach((before, after) -> assertEquals(after, normalizedUriFrom(before)));
    }
}