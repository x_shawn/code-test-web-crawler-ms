package monitor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.actuate.health.Health;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.*;
import static org.junit.Assert.*;
import static org.springframework.boot.actuate.health.Status.DOWN;
import static org.springframework.boot.actuate.health.Status.UP;

public class HealthIndicatorTest {
    private static final String CRAWL_DATA_FOLDER = "crawl_data";

    private static final Path APP_ROOT_PATH = Paths.get(".");
    private static final Path CRAWL_DATA_PATH = APP_ROOT_PATH.resolve(CRAWL_DATA_FOLDER);

    private AppHealthIndicator appHealthIndicator;

    @Before
    public void appRootPathPermissionCheck() {
        appHealthIndicator = new AppHealthIndicator(CRAWL_DATA_FOLDER);
        assertTrue(isReadable(APP_ROOT_PATH) && isExecutable(APP_ROOT_PATH));
    }

    @After
    public void resetCrawlDataPathPermission() {
        CRAWL_DATA_PATH.toFile().setReadable(true);
        CRAWL_DATA_PATH.toFile().setExecutable(true);
    }

    @Test
    public void shouldBeHealthyAndCrawlDataPathExists() throws IOException {
        // WHEN: monitoring the app health
        Health health = appHealthIndicator.health();

        // THEN: verify if app is UP and CRAWL_DATA_PATH exists for web crawling
        assertEquals(UP, health.getStatus());
        assertTrue(exists(CRAWL_DATA_PATH));
    }

    @Test
    public void shouldBeUnhealthyWhenCrawlDataPathBeingUnReadableOrUnExecutable() {
        // GIVEN: a situation where other apps had modified the permission of CRAWL_DATA_PATH
        CRAWL_DATA_PATH.toFile().setReadable(false);
        CRAWL_DATA_PATH.toFile().setExecutable(false);

        // WHEN: monitoring the app health
        Health health = appHealthIndicator.health();

        // THEN: verify if app is DOWN, due to CRAWL_DATA_PATH cannot be accessed
        assertEquals(DOWN, health.getStatus());
        assertFalse(isReadable(CRAWL_DATA_PATH));
        assertFalse(isExecutable(CRAWL_DATA_PATH));
    }
}
