﻿# YAWC: Yet Another Web Crawler
This README document aims to walk you through my personal solution - i.e. **YAWC**, to the web crawling challenge from Qantas.

# Requirement
As per requirement definition [here](https://drive.google.com/open?id=0B3eTIijK9JTxT0lobUdITmllb09lYk5hU1F2WWhwaTJ0V0J3), a RESTful API needs to be designed and developed to accept a given web URL, crawl the corresponding web page and its child pages, and respond with tree-structured JSON data consisting of at least 3 fields - i.e. `URL`, `Title`, and `Nodes`.

# Solution
To provide a proof of concept (**PoC**) solution, I follow the well-known **KIS** (keep-it-simple) principle and *focus on the minimum requirement only*. As a result, the current version of my solution has quite a few limitation, which I would elaborate on later.

## Build & Run
Before jumping into the solution *design & implementation* details, here let me firstly explain how to *build & run* **YAWC** application on your local machine. As such, you can have a *look & feel* on this toy prior to its engineering details.

### Pre-requisite
To play around my ugly toy, you need to ensure the following  to be met on your local environment.

 - **Java 8+**
 - **Gradle 4.6+**

Once the above has been installed, you can fetch the source code in a zip file [here](https://drive.google.com/file/d/1PJvNP_nN0TdcmCwotSCENXtj9PVLsOJe/view?usp=sharing); or `git clone` it from [its bitbucket repository](https://bitbucket.org/x_shawn/code-test-web-crawler-ms/src/master/)

Then under the root path of the above fetched project, open your *Terminal* app if you are on Mac OS X or any other Unix-alike OS; or *CMD* window if you are using Windows. After that, take a deep breath, then

### Just Run

    gradle bootRun

If you are curious about what the above Gradle task does, it goes through the following build pipeline: `compileJava -> processResources -> classes -> bootRun`. As you can see, this pipeline skips the unit test and package steps. Therefore, alternatively, you can firstly

### Build

    gradle build

The above command would go through the entire Java app build process, including unit test execution and Jar file package.  Then you can

### Run

    java -jar build/libs/${jarFileName}.jar

To try out our **YAWC** application, you need to construct a **HTTP POST request** as required by the following:

 - **header**: `Content-Type: application/json`
 - **body**: a JSON message with only 1 field, e.g. `{"crawlingUrl":"https://www.google.com.au"}` 

Then send it to **endpoint**: [http://localhost:8000/crawler/webPage](http://localhost:8000/crawler/webPage)

The very first time execution takes a while (45 seconds - 1 minute on Intel i5 dual core & 8GB RAM machine), due to the warm up. Then the repeated API call with the same request is pretty fast (< 100 milli-seconds).

**NOTE**: if you don't have any REST client, e.g. *postman* or *curl*, you can still try it out via our swagger UI - i.e. [http://localhost:8000/crawler/swagger-ui.html#/web45page45resource](http://localhost:8000/crawler/swagger-ui.html#/web45page45resource) on your web browser.

**NOTE 2**: if it does not work either way, ...hmmmm... call me ?

## Design
First of all, thanks for reading on ! after trying out my ugly toy ... you probably already bear some questions in mind, e.g. why crawling a simple Google homepage takes so long, why crawling some websites returns very limited data, etc. Before answering those questions, please allow me to point out that, **YAWC** is just a **PoC** toy focusing on the **minimum functionality requirement**, which is being developed *within my limited spare time*. As such, it has taken a quite few sacrifice.

The first sacrifice is the design. As we know, web crawling is an intensive time and resource consuming process, due to numerous pages on World Wide Web. Therefore, to my limited knowledge, an **ideal** design is to separate different concerns into different standalone services, particularly,

 - **URL Seeding** Service: collect the root or top level URLs for later crawling
 - **Crawling** Service: download and process web pages to extract interesting info
 - **Persistence** Service: store, index, cache the crawled page info for easier fetch
 - **API**: client-oriented facade exposing the crawled web info that is fetched from persistecen service

**Ideally**, each of the above should be designed, developed, released, scaled, and maintained as a separate service.

**Realistically**, to meet the minimum functionality requirement as a PoC within a limited time period, I've designed each of the above as a separate layer within a single service.  I will detail them through both class model and operation sequence

### Class Model
Here lists some of essential entities defined in our **YAWC** service.

 - **POJOs**: entities here represent the API input and output - i.e. **CrawlingRequest** and **WebPage**. The latter has a `List<WebPage> nodes` field to include its child pages

 - **WebPageResource**: this entity represents the above **API** layer exposing a RESTful API handling the request and response

 - **WebPageCrawler** & **CrawlingTask**: those 2 entities realise the above **Crawling Service** layer fetching a certain HTML document and converting it into a **WebPage** PoJo as above. To be more specific, **CrawlingTask** does the actual crawling work (e.g. fetching and converting) while **WebPageCrawler** configures the crawling strategy, e.g. maximum crawling links and depth, serial or concurrent crawling, etc.

 - **InMemoryPersistence**: this entity acts as the above **Persistence Service** layer storing and caching the crawled web page to serve the repeated, same web page crawling.

You may wonder where is the above **URL Seeding** layer; well, to stick to **KIS** principle, we skip it and accept the given URL from client; yet we do verify the given URL, normalise it for later crawling.

### Operation Sequence
To put the above entities together, we can see how they work with each other via the following sequence diagram.

```mermaid
sequenceDiagram
WebPageResource ->> WebPageResource: isValidUrl(url)?
WebPageResource ->> WebPageResource: no -> BAD_REQUEST
WebPageResource ->> WebPageCrawler: yes -> doCrawl(url)
WebPageCrawler ->> InMemoryPersistence: findByUrl(url) ?
WebPageCrawler ->> WebPageCrawler: yes -> return earlier cached
WebPageCrawler ->> WebPageCrawler: no -> addSeed(url)
WebPageCrawler -->> CrawlingTask: start
CrawlingTask ->> CrawlingTask: shouldVisit(url) ?
CrawlingTask ->> CrawlingTask: skip if invalid URL
CrawlingTask ->> CrawlingTask: fetch HTML content, e.g. title and links
CrawlingTask ->> CrawlingTask: build WebPage PoJo and set parent-child relation
CrawlingTask ->> InMemoryPersistence: save new WebPage PoJo
WebPageCrawler ->> InMemoryPersistence: findByUrl(url) again
```

The above design probably can answer part of your questions in mind. For instance, this synchronous, end-to-end interaction among multiple participants can explain why the responding time is quite long, as **WebPageResource** has to wait for **CrawlingTask** to finish the crawling process, which is time consuming as it has to continuously crawl the outgoing links. It also explains why the second API call with the same request is much faster, as the previous crawling result has been cached.

## Implementation

To implement the above design, I firstly choose the following.

### Development Stack

 - **Spring Boot 2**: as a general-purpose Java web development framework
 - **Crawl4J**: as a *handy* (not wholy-so, as it is not that easy to understand how it controls the whole crawling process) library for web page crawling
 - **Gradle**: as a less verbose project building tool

With the above development facilities, I do want to highlight and share some of my thinking during the implementation.

### Some Highlights

 - **Crawl4J** Vs. Other low-level HTML manipulation libraries

At the earlier stage of my implementation, I need to make a decision to choose a specific web crawling framework or some low-level HTML manipulation libraries. Each choice has its both benefits and cost. 

Unlike Spring, Crawl4J is a less-known framework specialising in web crawling. It can save us from those low-level work, e.g. continuously downloading web pages and their child pages. Yet, we lose the flexibility and control, e.g. app could fail silently as all exceptions have been caught internally and app code has no clue about what page is failed, skipped for what reason, etc. (we can only see that in the error log manually). Further, its Github site only provides a Quickstart and some demos without deeper insights into how the framework starts and controls the whole crawling process. I actually spent a significant period of time debugging certain issues. And by looking at its source code, its not-that-good threading management (e.g. it creates threads directly without using any ThreadPool) could contribute to the poor performance, to some degree.

By using other low-level HTML manipulation libraries, e.g. jsoup, we can gain the finer control on how the crawling process works, e.g. we could use ForkJoinPool to better tackle the recursive child page crawling task; we could audit the page crawling status, like what page is failed or skipped for what reason, etc.

By juggling with the above 2 choices, the reason why I still go for Crow4j is, again, PoC goal within a short period of time.

 - **POST** or **GET**

While you are going through my code, you may wonder why the RESTful API interface has been defined as using **POST** HTTP verb instead of **GET**. As per the requirement, aren't we just fetching the web info without creating or updating anything ?

Well, to answer that leads us to our REST resource definition. The **resource** **representation** in our **YAWC** application is **WebPage POJO in JSON format**. And we do **create** it when it is the **first time** when we are crawling the given URL. And despite the fact that we cache it for later repeatable fetch, the **interface** should tell the client that, each API call is **not idempotent**, which means, even the same API call with the same request info, the response could be different, because ideally we need to re-crawl the same web page even its URL is not changed, as its content may change. Therefore, given the **resource creation** involved and future **non-idempotent** refactoring work, the **POST** verb is chosen over others.

Another reason why POST is preferable is the tricky URL. Technically, if you define the RESTful API interface using GET verb, the target URL has to be part of your endpoint parameter list; and **the target URL itself could contain special characters**, e.g. ?, &, etc., which makes your own endpoint URL difficult to understand and process.

 - **HealthCheck**

**as a self-contained service, it should be deployment and release friendly**, which means we should bear **service monitoring and measurement** in mind, even though the main focus is functionality. In our case, to ensure crawling process work, we need to check if current runtime file system grants the permission to create and save temporary crawling files. This should be part of service health check from application perspective, on top of the infrastructure-level check. And it can be verified via endpoint: [http://localhost:8000/crawler/actuator/health](http://localhost:8000/crawler/actuator/health)

## Test

**Ideally**, I should have done all levels of testing, including unit test, function test, load test, etc. 

**In reality**, given the time limit and PoC nature, I've **provided as much coverage as possible of unit test** as part of implementation. Additionally, I've **done some manual function test** after implementation. Given no deployment and release need, I **skip the load test**.

## Limitation

Having tried out and explored the ins and outs of our **YAWC** application, here is a summary of its limitation, from the functionality perspective.

 - **HTML source elements only**:

This means **YAWC** does **not support the dynamic HTML element generation**, e.g. the morden react based, a single page web application cannot be crawled. The reason being that is, we simply download the source HTML content from the remote page; yet we do not execute any Javascript function to obtain the dynamic HTML elements; if we need to support that, that means our application is not only a RESTful API, but acts as a web browser.
 
 - **HTML document only**

This means **YAWC** does **not crawl any binary content**, e.g. image, audio, video, zip or tar file, etc.

 - **Valid URL only**

This means YAWC only crawls the given URL, if it is: 
1). an **absolute path**: unfortunately, our **YAWC** is not smart enough to supplement the relative URL in a web page with its root domain;

2). **not a binary file URL**: as explained above

3). be able to be **converted a syntactic URI**: to fetch the previously crawled web page saved by our `InMemoryPersistence`, the identity of a web page can only be `java.net.URI` instead of `java.lang.String` and `java.net.URL`, due to the URL equality check (e.g. special character escaped or encoded, etc.)

 - **Maximum crawling depth**: 5

This means there won't be more than 5 nested `nodes[]` within the JSON response body. The reason being that is the poor performance without the crawling depth limit. Given we are implementing a RESTful API over HTTP, the maximum connection timeout is 2 minutes. After testing on my local machine (i.e. Intel i5 dual core Mac), the acceptable crawling depth is 5. This along with the following limitation probably can explain why crawling some websites return more data, while others return less.

 - **Maximum outgoing links per page**: 6

This means **YAWC** only follows at most 6 links crawled from current page. The reason being that is the same as above. Having said that, though, those 2 flags are configurable.

# Future Work

I really appreciate your patience, if you are still reading on here ! In terms of the future work, I would say there are 2 levels - i.e. long-term and short-term.

In the long run, we need to re-design and re-work the whole system to lift those functional limitation and poor performance as mentioned above, in a real-world development.

To gain a glimpse into the future design, the following overview diagram is my best understanding at this moment

```mermaid
graph LR
A[RESTful API] -- reads --> B((Persistence Service))
C(Crawling Service) -- scheduled writes --> B
A -- new URLs --> D[URL Seeding Service]
D -- seeding --> E((Seeded URL Repo))
C -- fetch seeded URLs --> E
```
The detailed explanation of the above design can be mouthful; yet here the bottom line I want to draw is to separate different, unit of concerns (e.g. URL seeding, crawling, and content fetching) into different, standalone piece of micro-services. Therefore, each of them can evolve separately.

Even under the current PoC-versioned design, the short-term improvement can be made on the implementation. For instance, replace our own `InMemoryPersistence` with more mature caching solution, e.g. EhCache, to be able to configure the cache expiry time; thereby enabling us to re-crawl the same URL for the latest web content. In doing so, our RESTful API can be truely non-idempotent, which avoids confusing the client to cache our crawled result. Further, we could re-write the `WebPageCrawler` and `CrawlingTask` using a low-level HTML library; then benchmark the performance against current Crawl4J version.

To wrap up, **YAWC** is a PoC solution to the web crawling challenge from Qantas. I've given my best to realise the minimum functional requirement within a short period of time. As such, a lot of improvement can be made on top of that. I'm looking forward to discussing that with you onsite :-).


# Referernce

> https://github.com/yasserg/crawler4j#quickstart
> https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/html/#packaging-executable-jars
> https://stackoverflow.com/questions/3771081/proper-way-to-check-for-url-equality
